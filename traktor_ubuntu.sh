#!/bin/bash

echo -e "Traktor V3\n"


check_Tor=`apt list --installed 2>&1 | grep "^tor/" | awk {'print $1'}`
SED_Tor=`echo ${check_Tor} | sed -e "/tor/s/\/.*//"`
if [[ $SED_tor == "tor" ]]; then
    read -p "do you want install tor? " tor_q
    if [ "$tor_q" == "y" ] || [ "$tor_q" == "Y" ] || [ "$tor_q" == "" ]; then
        sudo apt install tor -y
    else
        exit 1
    fi
fi

check_obfs4=`apt list --installed 2>&1 | grep "^obfs4proxy/" | awk {'print $1'}`
SED_obfs4=`echo ${check_obfs4} | sed -e "/obfs4proxy/s/\/.*//"`
if ! [[ $SED_obfs4 == "obfs4proxy" ]]; then
    read -p "do you want install obfs4proxy?[Y/n] " obfs4_q
    if [ "$obfs4_q" == "y" ] || [ "$obfs4_q" == "Y" ] || [ "$obfs4_q" == "" ]; then
        sudo apt install obfs4proxy -y
    else
        exit
    fi
fi

function bridge_log {
sudo sed -i -e "/#Log notice file \/var\/log\/tor\/notices.log/s/^#//g" /etc/tor/torrc

log_line=`cat /etc/tor/torrc | grep "Log notice file /var/log/tor/notices.log"`

if ! [[ $log_line == "Log notice file /var/log/tor/notices.log" ]]; then
    echo -e "\nLog notice file /var/log/tor/notices.log" | sudo tee -a /etc/tor/torrc > /dev/null
fi
}

function bridge_use {
use_bridge=`cat /etc/tor/torrc | grep "^UseBridges 1"`

if ! [[ $use_bridge == "UseBridges 1" ]]; then
    echo -e "\nUseBridges 1" | sudo tee -a /etc/tor/torrc > /dev/null
fi
}

function bridge_obfs4 {
obfs4=`cat /etc/tor/torrc | grep "^ClientTransportPlugin obfs4 exec /usr/bin/obfs4proxy"`
if ! [[ $obfs4 == "ClientTransportPlugin obfs4 exec /usr/bin/obfs4proxy" ]]; then
    echo -e "\nClientTransportPlugin obfs4 exec /usr/bin/obfs4proxy" | sudo tee -a /etc/tor/torrc > /dev/null
fi
}

trap printout SIGINT
printout() {
    echo -e "\n\ngoodbye."
    exit
}
while true; do
    bridge_log
    bridge_use
    read -p 'Paste Bridge [for exit, press "CTRL + C"]: ' bridge
    bridge_Check=`echo $bridge | grep "iat-mode=[012]"`
    if [[ $? == "0" ]]; then
	bridge_Check2=`echo $bridge | awk {'print $1'}`
	if [[ $bridge_Check2 == "Bridge" ]]; then
	    echo $bridge | sudo tee -a /etc/tor/torrc > /dev/null
	else
	    bridge_Check3=`echo $bridge | awk {'print $1'}`
	    if [[ $bridge_Check3 == "bridge" ]]; then
		echo ${bridge_Check3} | sed -e "s/bridge/Bridge/g" | sudo tee -a /etc/tor/torrc > /dev/null
	    else
		echo "Bridge $bridge" | sudo tee -a /etc/tor/torrc > /dev/null
	    fi
	fi
    else
	echo "Wrong Bridge!"
    fi
    bridge_obfs4
done
