#!/bin/bash

function check_Tor_Obfs_ufw {
check_Tor=`apt list --installed 2>&1 | grep "^tor/" | awk {'print $1'}`
SED_Tor=`echo ${check_Tor} | sed -e "/tor/s/\/.*//"`
if [[ $SED_tor == "tor" ]]; then
    read -p "do you want install tor? " tor_q
    if [ "$tor_q" == "y" ] || [ "$tor_q" == "Y" ] || [ "$tor_q" == "" ]; then
        sudo apt install tor -y
    else
        exit 1
    fi
fi

check_obfs4=`apt list --installed 2>&1 | grep "^obfs4proxy/" | awk {'print $1'}`
SED_obfs4=`echo ${check_obfs4} | sed -e "/obfs4proxy/s/\/.*//"`
if ! [[ $SED_obfs4 == "obfs4proxy" ]]; then
    read -p "do you want install obfs4proxy?[Y/n] " obfs4_q
    if [ "$obfs4_q" == "y" ] || [ "$obfs4_q" == "Y" ] || [ "$obfs4_q" == "" ]; then
        sudo apt install obfs4proxy -y
    else
        exit
    fi
fi

check_ufw=`apt list --installed 2>&1 | grep "^ufw/" | awk {'print $1'}`
SED_ufw=`echo ${check_ufw} | sed -e "/ufw/s/\/.*//"`
if [[ $SED_ufw == "ufw" ]]; then
    ufw_Installed=true
fi
}

function info_Single {
read -p 'Enter Nickname:[press "Enter" for skip] (show in log and other. Nicknames must be between "1 and 19 characters" inclusive, and must contain only the characters "[a-zA-Z0-9]") ' nickname_q
if ! [ "$nickname_q" == "" ]; then
    nickname="Nickname $nickname_q"
fi
read -p 'Enter Email:[press "Enter" for skip] (maybe show in log) ' email_q
if ! [ "$email_q" == "" ]; then
    email="ContactInfo $email_q"
fi
}

function info {
echo -e "\n(step 1/3 for iat-mode=0)"
read -p 'Enter Nickname:[press "Enter" for skip] (show in log and other. Nicknames must be between "1 and 19 characters" inclusive, and must c
ontain only the characters "[a-zA-Z0-9]") ' nickname_q
if ! [ "$nickname_q" == "" ]; then
    nickname="Nickname $nickname_q"
fi
read -p 'Enter Email for contact:[press "Enter" for skip] (maybe show in log) ' email_q
if ! [ "$email_q" == "" ]; then
    email="ContactInfo $email_q"
fi
echo -e "\n(step 2/3 for iat-mode=1)"
read -p 'Enter Nickname:[press "Enter" for skip] (show in log and other. Nicknames must be between "1 and 19 characters" inclusive, and must c
ontain only the characters "[a-zA-Z0-9]") ' nickname_q2
if ! [ "$nickname_q2" == "" ]; then
    nickname2="Nickname $nickname_q2"
fi
read -p 'Enter Email for contact:[press "Enter" for skip] (maybe show in log) ' email_q2
if ! [ "$email_q2" == "" ]; then
    email2="ContactInfo $email_q2"
fi
echo -e "\n(step 3/3 for iat-mode=2)"
read -p 'Enter Nickname:[press "Enter" for skip] (show in log and other. Nicknames must be between "1 and 19 characters" inclusive, and must c
ontain only the characters "[a-zA-Z0-9]") ' nickname_q3
if ! [ "$nickname_q3" == "" ]; then
    nickname3="Nickname $nickname_q3"
fi
read -p 'Enter Email for contact:[press "Enter" for skip] (maybe show in log) ' email_q3
if ! [ "$email_q3" == "" ]; then
    email3="ContactInfo $email_q3"
fi
}

function bridge_Listen_Single {
echo -e "\n"
read -p 'Enter a number for "ServerTransportListenAddr" port: (this is your birdge port) ' bridgeport
}

function bridge_Listen {
echo -e "\n(Bridge 1)"
read -p 'Enter a number for "ServerTransportListenAddr" port: (this is your ibirdge port) ' bridgeport
echo -e "\n(Bridge 2)"
read -p 'Enter a number for "ServerTransportListenAddr" port: (this is your ibirdge port) ' bridgeport2
echo -e "\n(Bridge 3)"
read -p 'Enter a number for "ServerTransportListenAddr" port: (this is your ibirdge port) ' bridgeport3
}

function bridge_Public_Single {
echo -e "\n"
read -p 'do you want public this bridge?[Y/n] (Attention: This may cause the server to be blocked in countries where there is censorship! defatult: "no") ' public
if [ "$public" == "y" ] || [ "$public" == "Y" ]; then
    public="1"           
else
    public="0"
fi
}

function bridge_Public {
echo -e "\n(Bridge 1)"
read -p 'do you want public this bridge?[Y/n] (Attention: This may cause the server to be blocked in countries where there is censorship! defatult: "no") ' public
if [ "$public" == "y" ] || [ "$public" == "Y" ]; then
    public="1"           
else
    public="0"
fi
echo -e "\n(Bridge 2)"
read -p 'do you want public this bridge?[Y/n] (Attention: This may cause the server to be blocked in countries where there is censorship! defatult: "no") ' public2
if [ "$public2" == "y" ] || [ "$public2" == "Y" ]; then
    public2="1"
else
    public2="0"
fi
echo -e "\n(Bridge 3)"
read -p 'do you want public this bridge?[Y/n] (Attention: This may cause the server to be blocked in countries where there is censorship! defatult: "no") ' public3
if [ "$public3" == "y" ] || [ "$public3" == "Y" ]; then
    public3="1"
else
    public3="0"
fi
}

function bridge_Single {
    if [ "$choose" == "1" ]; then
        iatmode="0"
    elif [ "$choose" == "2" ]; then
        iatmode="1"
    elif [ "$choose" == "3" ]; then
        iatmode="2"
    else
        iatmode="0"
    fi

echo -e "DataDirectory /var/lib/tor/
PidFile /var/run/tor/tor.pid
Log notice file /var/log/tor/notices.log
Log warn file /var/log/tor/warn.log
Log err file /var/log/tor/error.log

PublishServerDescriptor $public
SocksPort 0
ORPort auto
ExtORPort auto
BridgeRelay 1
Exitpolicy reject *:*
ServerTransportOptions obfs4 iat-mode=$iatmode
ServerTransportListenAddr obfs4 [::]:$bridgeport
ServerTransportPlugin obfs4 exec /usr/bin/obfs4proxy managed

$nickname
$email" | sudo tee /etc/tor/torrc > /dev/null
}

function bridge_Zero {
echo -e "DataDirectory /var/lib/tor/
PidFile /var/run/tor/tor.pid
Log notice file /var/log/tor/notices.log
Log warn file /var/log/tor/warn.log
Log err file /var/log/tor/error.log

PublishServerDescriptor $public
SocksPort 0
ORPort auto
ExtORPort auto
BridgeRelay 1
Exitpolicy reject *:*
ServerTransportOptions obfs4 iat-mode=0
ServerTransportListenAddr obfs4 [::]:$bridgeport
ServerTransportPlugin obfs4 exec /usr/bin/obfs4proxy managed

$nickname
$email" | sudo tee /etc/tor/torrc > /dev/null
}

function bridge_One {
echo -e "DataDirectory /var/lib/tor/1
PidFile /var/run/tor/tor1.pid
Log notice file /var/log/tor/notices1.log
Log warn file /var/log/tor/warn1.log
Log err file /var/log/tor/error1.log

PublishServerDescriptor $public2
SocksPort 0
ORPort auto
ExtORPort auto
BridgeRelay 1
Exitpolicy reject *:*
ServerTransportOptions obfs4 iat-mode=1
ServerTransportListenAddr obfs4 [::]:$bridgeport2
ServerTransportPlugin obfs4 exec /usr/bin/obfs4proxy managed

$nickname2
$email2" | sudo tee /etc/tor/torrc1 > /dev/null
}

function bridge_Two {
echo -e "DataDirectory /var/lib/tor/2
PidFile /var/run/tor/tor2.pid
Log notice file /var/log/tor/notices2.log
Log warn file /var/log/tor/warn2.log
Log err file /var/log/tor/error2.log

PublishServerDescriptor $public3
SocksPort 0
ORPort auto
ExtORPort auto
BridgeRelay 1
Exitpolicy reject *:*
ServerTransportOptions obfs4 iat-mode=2
ServerTransportListenAddr obfs4 [::]:$bridgeport3
ServerTransportPlugin obfs4 exec /usr/bin/obfs4proxy managed

$nickname3
$email3" | sudo tee /etc/tor/torrc2 > /dev/null
}

function Tor_Restart_Single {
echo -e "\n"
sudo service tor restart
}

function Tor_Restart {
echo -e "\n"
sudo service tor restart
sudo -u debian-tor tor -f /etc/tor/torrc1 &
PID=$!
sleep 2
sudo kill -INT $PID
sudo -u debian-tor tor -f /etc/tor/torrc2 &
PID=$!
sleep 2
sudo kill -INT $PID
}

function open_Port_Single {
if [ "$ufw_Installed" == "true" ]; then
    echo -e "\n"
    read -p "We detected 'UFW Firewall' in system. do you like open bridges port via UFW?[Y/n] " ufw_Ports
    if [ "$ufw_Ports" == "y" ] || [ "$ufw_Ports" == "Y" ] || [ "$ufw_Ports" == "" ]; then
        sudo ufw allow $bridgeport
    else
        echo -e "\nplease open this ports: $bridgeport"
    fi
else
    echo -e "\nplease open this ports: $bridgeport"
fi
}


function open_Port {
if [ "$ufw_Installed" == "true" ]; then
    echo -e "\n"
    read -p "We detected 'UFW Firewall' in system. do you like open bridges port via UFW?[Y/n] " ufw_Ports
    if [ "$ufw_Ports" == "y" ] || [ "$ufw_Ports" == "Y" ] || [ "$ufw_Ports" == "" ]; then
	sudo ufw allow $bridgeport
	sudo ufw allow $bridgeport2
	sudo ufw allow $bridgeport3
    else
	echo -e "\nplease open this ports: $bridgeport, $bridgeport2 and $bridgeport3"
    fi
else
    echo -e "\nplease open this ports: $bridgeport, $bridgeport2 and $bridgeport3"
fi
}

function bridge_Print_Single {
bridge_obfs4=`sudo cat /var/lib/tor/pt_state/obfs4_bridgeline.txt | grep "^Bridge"`
server_IP=`hostname -I | awk {'print $1'}`
bridge_IP=`echo ${bridge_obfs4} | sed -e "s/<IP ADDRESS>/$server_IP/"`
bridge_Port=`sudo cat /var/lib/tor/state | grep obfs | awk {'print $3'} | sed -e "s/\[::]://"`
bridge_Obfs=`echo ${bridge_IP} | sed -e "s/<PORT>/$bridge_Port/"`
bridge_Finger=`sudo cat /var/lib/tor/fingerprint | awk {'print $2'}`
bridge=`echo ${bridge_Obfs} | sed -e "s/<FINGERPRINT>/$bridge_Finger/"`
echo -e "\nYour Bridge: $bridge"
}

function bridge_Print {
bridge_obfs4=`sudo cat /var/lib/tor/pt_state/obfs4_bridgeline.txt | grep "^Bridge"`
server_IP=`hostname -I | awk {'print $1'}`
bridge_IP=`echo ${bridge_obfs4} | sed -e "s/<IP ADDRESS>/$server_IP/"`
bridge_Port=`sudo cat /var/lib/tor/state | grep obfs | awk {'print $3'} | sed -e "s/\[::]://"`
bridge_Obfs=`echo ${bridge_IP} | sed -e "s/<PORT>/$bridge_Port/"`
bridge_Finger=`sudo cat /var/lib/tor/fingerprint | awk {'print $2'}`
bridge=`echo ${bridge_Obfs} | sed -e "s/<FINGERPRINT>/$bridge_Finger/"`
echo -e "\n\nYour Bridge(1): $bridge"

bridge2_obfs4=`sudo cat /var/lib/tor/1/pt_state/obfs4_bridgeline.txt | grep "^Bridge"`
server_IP2=`hostname -I | awk {'print $1'}`
bridge_IP2=`echo ${bridge2_obfs4} | sed -e "s/<IP ADDRESS>/$server_IP2/"`
bridge_Port2=`sudo cat /var/lib/tor/1/state | grep obfs | awk {'print $3'} | sed -e "s/\[::]://"`
bridge2_Obfs=`echo ${bridge_IP2} | sed -e "s/<PORT>/$bridge_Port2/"`
bridge_Finger2=`sudo cat /var/lib/tor/1/fingerprint | awk {'print $2'}`
bridge2=`echo ${bridge2_Obfs} | sed -e "s/<FINGERPRINT>/$bridge_Finger2/"`
echo -e "\nYour Bridge(2): $bridge2"

bridge3_obfs4=`sudo cat /var/lib/tor/2/pt_state/obfs4_bridgeline.txt | grep "^Bridge"`
server_IP3=`hostname -I | awk {'print $1'}`
bridge_IP3=`echo ${bridge3_obfs4} | sed -e "s/<IP ADDRESS>/$server_IP3/"`
bridge_Port3=`sudo cat /var/lib/tor/2/state | grep obfs | awk {'print $3'} | sed -e "s/\[::]://"`
bridge3_Obfs=`echo ${bridge_IP3} | sed -e "s/<PORT>/$bridge_Port3/"`
bridge_Finger3=`sudo cat /var/lib/tor/2/fingerprint | awk {'print $2'}`
bridge3=`echo ${bridge3_Obfs} | sed -e "s/<FINGERPRINT>/$bridge_Finger3/"`
echo -e "\nYour Bridge(3): $bridge3"
}

function crontab_tor {
    echo -e "\n\nPlease type 'crontab -e -u debian-tor' and input this lines:
@reboot /usr/bin/tor -f /etc/tor/torrc1
@reboot /usr/bin/tor -f /etc/tor/torrc2"
}

echo -e "1) Create Tor bridge (only create 'iat-mode=0')
2) Create Tor bridge (only create 'iat-mode=1')
3) Create Tor bridge (only create 'iat-mode=2')
4) Create Tor bridge (iat-mode=0, iat-mode=1 and iat-mode=2) [Default]\n"

read -p "whats your choose? " choose
if [ "$choose" == "1" ]; then
    check_Tor_Obfs_ufw
    info_Single
    bridge_Listen_Single
    bridge_Public_Single
    bridge_Single
    open_Port_Single
    Tor_Restart_Single
    bridge_Print_Single
elif [ "$choose" == "2" ]; then
    check_Tor_Obfs_ufw
    info_Single
    bridge_Listen_Single
    bridge_Public_Single
    bridge_Single
    open_Port_Single
    Tor_Restart_Single
    bridge_Print_Single
elif [ "$choose" == "3" ]; then
    check_Tor_Obfs_ufw
    info_Single
    bridge_Listen_Single
    bridge_Public_Single
    bridge_Single
    open_Port_Single
    Tor_Restart_Single
    bridge_Print_Single
elif [ "$choose" == "4" ] || [ "$choose" == "" ]; then
    check_Tor_Obfs_ufw
    info
    bridge_Listen
    bridge_Public
    bridge_Zero
    bridge_One
    bridge_Two
    Tor_Restart
    open_Port
    bridge_Print
    crontab_tor
else
    echo -e "Abort!"
fi
